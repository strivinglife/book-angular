# Add Jest
```bash
npm install --save-dev jest jest-preset-angular @types/jest
# For Angular v13.
npm install --save-dev jest-preset-angular@next
```

New scripts in **package.json**:
```json
"jest": "jest",
"jest:coverage": "jest --coverage",
"jest:coverageWatch": "jest --coverage --watchAll"
```

New **setup-jest.ts** file in the root directory:
```typescript
import 'jest-preset-angular/setup-jest';
```

New **jest.config.js** file in the root directory:
```javascript
// jest.config.js
module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
  testPathIgnorePatterns: [
    '/node_modules/',
    '<rootDir>/src/test.ts'
  ],
  // An array of glob patterns indicating a set of files for which coverage information should be collected
  collectCoverageFrom: [
    "<rootDir>/src/**/*.ts",
    // Exclude main Angular files.
    "!<rootDir>/src/main.ts",
    "!<rootDir>/src/polyfills.ts",
    "!<rootDir>/src/environments/**",
    // Exclude Karma file.
    "!<rootDir>/src/test.ts",
    "!<rootDir>/src/**/*.d.ts",
    '!<rootDir>/node_modules/**',
  ],
  coverageReporters: [
    //"json",
    "text", // Command line output
    "text-summary", // Command line output of the summary
    "lcov",
    //"clover"
  ],
};
```

You may need to add the following in `module.exports` if it can't find your imports:
```javascript
  modulePaths: [ "<rootDir>" ]
```

New ignore in **.gitignore**:
```
# Jest
/coverage
```

Update **tsconfig.spec.json** in root directory:
```json
    "types": [
      "jasmine",
      "jest"
    ]
```

## Test
```bash
npm run jest
```
