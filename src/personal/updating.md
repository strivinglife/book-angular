# Updating
See the [Angular Update Guide](https://update.angular.io/) for version-specific steps.

## Angular
```bash
ng update @angular/cli @angular/core
# If using Angular Material (also updates cdk package):
ng update @angular/material
```

### Updating to latest patch version
```bash
ng update @angular/cli@^<major_version> @angular/core@^<major_version> @angular/material@^<major_version>
# Update, ignoring pending changes
ng update @angular/cli@^<major_version> @angular/core@^<major_version> @angular/material@^<major_version> --allow-dirty
```

## ESLint
```bash
ng update @angular-eslint/schematics
npm run eslint
```

## Jest
```bash
npm install @types/jest jest jest-preset-angular
npm run jest
```

## Stylelint
```bash
npm install --save-dev stylelint stylelint-config-sass-guidelines stylelint-config-standard
npm run stylelint
```
