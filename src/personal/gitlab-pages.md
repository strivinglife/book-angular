# Configure for GitLab Pages
New script in **package.json**:
```json
"buildProd": "ng build --base-href=/project-name/ --output-path=public",
```

Add new **.gitlab-ci.yml** in root directory:
```yaml
image: node:14.17.6
pages:
  cache:
    paths:
      - node_modules/
  script:
    - npm install -g @angular/cli@12.2.9
    - npm install
    - npm run buildProd
  artifacts:
    paths:
      - public
  only:
    - main
    - pages
```
