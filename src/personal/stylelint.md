# Add Stylelint
```bash
npm init stylelint
npm install --save-dev stylelint-config-standard-scss
```

New script in **package.json**:
```json
"stylelint": "stylelint \"src/**/*.scss\" --cache --formatter verbose --output-file stylelint.log || (exit 0)"
```

New **.stylelintrc.js** file in the root directory:
```javascript
{
  "extends": ["stylelint-config-standard", "stylelint-config-standard-scss"]
}
```

New ignore in **.gitignore**:
```
# Stylelint
.stylelintcache
stylelint.log
```

## Test
```bash
npm run stylelint
```

## VS Code
- Install Stylelint official extension.
- Update settings to include `scss` files.
