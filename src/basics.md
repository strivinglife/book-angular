# Basics
Written as of Angular 12+.

## Install the Angular CLI
```bash
npm install -g @angular/cli
```

## Help/available commands
```bash
ng help
ng ___ --help
```

## Create a new site/project
```bash
ng new new-project-name
# Use SCSS stylesheet format.
ng new new-project-name --style scss
# This option can be used if there's an issue during npm install.
ng new new-project-name --skip-install
# Use a custom prefix for selector names.
ng new new-project-name --prefix ___
# Create with a specific Angular version.
npx @angular/cli@12 new new-project-name
```

## Start a development server
Runs on port 4200 by default.

```bash
ng serve
# Opens the site in the default browser.
ng serve --open
ng serve -o
```

## Code scaffolding
See [ng generate](https://angular.io/cli/generate).

```bash
# Create a new component.
ng generate component heroes
ng generate component hero-detail
ng g c input-button-unit
ng g c products/product-detail
# Create a component in the current directory, without creating a directory.
ng g c product-detail --flat
ng g c products/product-detail --flat
# Preview creating a new component.
ng g c messages --dry-run
ng g c messages -d
# Create a new service.
ng generate service hero
ng generate service message
ng g s services/todo-list
# Create an interface
ng g i interfaces/todo-item
# Create a new module, putting it into AppModule.
ng g m products/product --flat -m app
# Create a new module, with routing.
ng g m admin-stuff --flat --routing
```

## Build the project
```bash
# Puts in dist/ by default.
ng build
```

## Add an in-memory web API
For mocking up a backend.


```bash
npm install angular-in-memory-web-api --save
```

```bash
# Generate a module in src/app (flat) and register it in AppModule imports (module=app)
ng generate module app-routing --flat --module=app
ng generate component dashboard
# In-memory Web API
npm install angular-in-memory-web-api --save
ng generate service InMemoryData
ng generate component hero-search
```

## Angular Material

- [Searchable Material Symbols](https://fonts.google.com/icons?icon.platform=android)
    - Use the Android name.

```bash
ng add @angular/material
```
